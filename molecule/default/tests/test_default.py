import os
import testinfra.utils.ansible_runner
import pytest


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


@pytest.mark.parametrize("name", ["tsc", "pon"])
def test_kernel_modules_present(host, name):
    # module should automatically be loaded by udev
    # if the required PCI card is present
    # it's not loaded in the VM as there is no PCI card
    # we just check it's present
    if str(host).find("ess-linux") >= 0:
        bin_path = ""
    else:
        bin_path = "/usr"
    cmd = host.run(f"{bin_path}/sbin/modinfo {name}")
    assert f"{name}.ko" in cmd.stdout
